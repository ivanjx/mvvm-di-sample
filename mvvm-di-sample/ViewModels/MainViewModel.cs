﻿using mvvm_di_sample.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace mvvm_di_sample.ViewModels
{
    // Read more at login view model.
    public class MainViewModel : ViewModelBase
    {
        IAuthService m_authService;
        UserInfoViewModel m_userInfoVM;

        public ICommand LogoutCommand
        {
            get;
            private set;
        }

        // Read more at login view model.
        public MainViewModel(
            IAuthService authService,
            UserInfoViewModel userInfoViewModel)
        {
            m_authService = authService;
            m_userInfoVM = userInfoViewModel;

            LogoutCommand = new RelayCommand(
                _ => Logout());
        }

        async void Logout()
        {
            // Logging out.
            await m_authService.LogoutAsync();

            // Tell the app that we are logged out.
            m_userInfoVM.IsLoggedIn = false;

            // Magic! (again)
        }
    }
}
