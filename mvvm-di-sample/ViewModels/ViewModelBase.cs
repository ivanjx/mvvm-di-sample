﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_di_sample.ViewModels
{
    // Use this class for all the view models.
    // Just copy and paste this class.
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyChange(string propName)
        {
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(propName));
        }
    }
}
