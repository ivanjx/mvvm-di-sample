﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_di_sample.ViewModels
{
    public class UserInfoViewModel : ViewModelBase
    {
        bool m_isLoggedIn;

        public bool IsLoggedIn
        {
            get
            {
                // Return the private field.
                return m_isLoggedIn;
            }
            set
            {
                // Set the private field.
                m_isLoggedIn = value;

                // Notify the 'listeners'
                NotifyChange(nameof(IsLoggedIn));

                // nameof(IsLoggedIn) == "IsLoggedIn"
            }
        }
    }
}
