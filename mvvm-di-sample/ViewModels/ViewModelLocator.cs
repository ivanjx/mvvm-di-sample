﻿using mvvm_di_sample.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_di_sample.ViewModels
{
    // This class will be used by the view xaml files to get the view model instances.
    public class ViewModelLocator
    {
        // List all of your view models here.
        public UserInfoViewModel UserInfoViewModel
        {
            get
            {
                // Resolve from services since we injected this into Service.cs class.
                return Service.Resolve<UserInfoViewModel>();
            }
        }

        public LoginViewModel LoginViewModel
        {
            get
            {
                // Resolve from services.
                return Service.Resolve<LoginViewModel>();
            }
        }

        public MainViewModel MainViewModel
        {
            get
            {
                return Service.Resolve<MainViewModel>();
            }
        }
    }
}
