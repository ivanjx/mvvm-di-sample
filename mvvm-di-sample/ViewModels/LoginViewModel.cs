﻿using mvvm_di_sample.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace mvvm_di_sample.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        IAuthService m_authService;
        UserInfoViewModel m_userInfoVM;

        string m_username;
        string m_password;

        public string Username
        {
            get
            {
                // Return the private field.
                return m_username;
            }
            set
            {
                // Set the private field.
                m_username = value;

                // Notify the listeners.
                NotifyChange(nameof(Username));
            }
        }

        public string Password
        {
            get
            {
                // Return the private field.
                return m_password;
            }
            set
            {
                // Set the private field.
                m_password = value;

                // Notify the listeners.
                NotifyChange(nameof(Password));
            }
        }

        public ICommand LoginCommand
        {
            // Like a button.Click event but much simpler.
            get;
            private set;
        }

        public LoginViewModel(
            IAuthService authService,
            UserInfoViewModel userInfoVM)
        {
            // Since auth service and user info view model are already injected in Service.cs class,
            // now we can just get it from the constructor and use it inside this login view model class.
            m_authService = authService;
            m_userInfoVM = userInfoVM;

            // Add command handler.
            LoginCommand = new RelayCommand(
                _ => Login());
        }

        async void Login()
        {
            // Validate username and password here.
            // blabla();

            // Then call login.
            await m_authService.LoginAsync(Username, Password);

            // Tell the app that we are logged in.
            m_userInfoVM.IsLoggedIn = true;

            // Magic!
            MessageBox.Show(
                string.Format(
                    "Your username is: {0}\nAnd your password is: {1}",
                    Username,
                    Password));

            // Optional:
            // If we want to clear the login form after logged in just:
            Username = string.Empty;
            Password = string.Empty;
        }
    }
}
