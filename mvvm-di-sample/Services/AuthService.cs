﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_di_sample.Services
{
    // Interface
    public interface IAuthService
    {
        Task LoginAsync(string username, string password);
        Task LogoutAsync();
    }

    // Implementation
    public class AuthService : IAuthService
    {
        public async Task LoginAsync(string username, string password)
        {
            // Call server or anything.
        }

        public async Task LogoutAsync()
        {
            // Call server or anything.
        }
    }
}
