﻿using mvvm_di_sample.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_di_sample.Services
{
    public static class Service
    {
        static IKernel m_kernel;

        public static void InitializeServices()
        {
            // Just need to call this function once after start.
            m_kernel = new StandardKernel();

            // Injecting auth service.
            // In singleton means the class AuthService will be only initialized once (like a static class).
            m_kernel.Bind<IAuthService>().To<AuthService>().InSingletonScope();

            // Injecting view models.
            m_kernel.Bind<UserInfoViewModel>().ToSelf().InSingletonScope();
            m_kernel.Bind<LoginViewModel>().ToSelf().InSingletonScope();
            m_kernel.Bind<MainViewModel>().ToSelf().InSingletonScope();
        }

        public static T Resolve<T>()
        {
            // Use this function to get class instance of an interface.
            // eg. call Resolve<IAuthService>() -> we will get an instance of AuthService.
            return m_kernel.Get<T>();
        }
    }
}
