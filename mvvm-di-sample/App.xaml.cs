﻿using mvvm_di_sample.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace mvvm_di_sample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // App entry point.
            // Initializing services and view models.
            Service.InitializeServices();

            // Showing mainwindow.
            new MainWindow().Show();
        }
    }
}
